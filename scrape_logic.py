"""
Uses praw to scrape Reddit post data, store with pandas and persist in MongoDB
"""

from pymongo import MongoClient
from bson import objectid
import pandas as pd
import datetime as dt
import praw

mongo_client = MongoClient().reddit_scraped_items # Create MongoDB client
collection_name = input("Please enter sub to scrape: ") # Ask user for sub to scrape, set as var
db_collection_name = mongo_client[collection_name] # Set sub as collection name, to organize DB entries by sub

# Enter Reddit API details to create praw client

reddit_client = praw.Reddit(
    client_id='Insert your client ID here',
    client_secret='Insert your secret here',
    username='Insert your username here',
    password='Insert your password here',
    user_agent='reddit_scraper v.01'
)

subreddit = reddit_client.subreddit(str(collection_name)) # Set subreddit
top_posts = subreddit.top(limit=100) # Subset posts by top
hot_posts = subreddit.hot(limit=100) # Subset posts by hot

# Init dict to hold post data

data_dict = {
    'title': [],
    'score': [],
    'id': [],
    'url': [],
    'comms_num': [],
    'created': [],
    'body': []
}

# Loop through posts and append data to dict

for post in hot_posts:
    data_dict['title'].append(post.title)
    data_dict['score'].append(post.score)
    data_dict['id'].append(post.id)
    data_dict['url'].append(post.url)
    data_dict['comms_num'].append(post.num_comments)
    data_dict['created'].append(post.created)
    data_dict['body'].append(post.selftext)

# Create df from dict

data = pd.DataFrame(data_dict)

# Helper function to convert UNIX time to gregorian

def get_date(created):
    return dt.datetime.fromtimestamp(created)

_timestamp = data['created'].apply(get_date) # Apply helper function to 'created' col
data = data.assign(timestamp=_timestamp) # Update values in df 

insert_to_db = data.to_dict(orient='records') # Convert back into dict to prepare for insert
db_collection_name.insert_many(insert_to_db) # Insert into MongoDB
